from datetime import datetime
from loguru import logger


def format_time(start_time, end_time):
    """
    format_time function calculates the duration between the start time and the end time
    :param start_time: When the counter first started
    :param end_time: When the counter ended
    :return: Return the difference in hours and minutes
    """
    difference = end_time - start_time
    # Calculate the difference in hours
    duration_hours = difference.total_seconds() // 3600
    # Calculate the difference in minutes
    duration_minutes = (difference.total_seconds() % 3600) // 60
    logger.info(f"{duration_hours}:{duration_minutes}")
    # return duration_hours, duration_minutes
    return f"{duration_hours}:{duration_minutes}"


def get_time():
    """
    get_time function is just set up to get the current time
    :return: Returns the current time
    """
    current_time = datetime.now()
    logger.info(current_time)
    return current_time
