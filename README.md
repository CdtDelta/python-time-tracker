# Python Time Tracker

Just a simple Python GUI that lets me track time that I'm working on things. Saves me from writing down start and stop times.

Big thank you to Mike Driscoll for the help with ObjectListView