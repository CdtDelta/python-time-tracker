"""
This is just a simple Python GUI app to track when I start and stop things. The plan is to have it list
the start and stop values below the timer, and give me the duration between the two.

Big thanks to Mike Driscoll for the help with ObjectListView

Python 3 and wxPython 4 required

By T. Yarrish
Version 0.1

TODO: Add a 15 minute notification to enter in notes
"""
import wx
from ObjectListView import ObjectListView, ColumnDefn
from timestamp import timestamp


class TimeObj:
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.duration = timestamp.format_time(start, end)


class TimePanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)
        self.data = []
        start_btn = wx.ToggleButton(self, label="Start Timer", size=(350, 150))
        start_btn.Bind(wx.EVT_TOGGLEBUTTON, self.OnToggle)
        self.dataOlv = ObjectListView(self, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.update_ui()
        self.start_time = None
        self.end_time = None

        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(start_btn, 1, wx.ALIGN_CENTER | wx.SHAPED)
        main_sizer.Add(self.dataOlv, 1, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(main_sizer)
        main_sizer.Fit(parent)
        self.Layout()

    def OnToggle(self, event):
        """
        This checks the state of the toggle button and either starts or stops the timer
        :param event: Is the toggle button pressed or no?
        :return:
        """
        state = event.GetEventObject().GetValue()

        if state:
            print("Running")
            # Get start time of event
            self.start_time = timestamp.get_time()
            event.GetEventObject().SetLabel("Click to Stop Timer")
            # self.WriteNotesCheck()
        else:
            print("Stopped")
            # Get stop time of event
            self.end_time = timestamp.get_time()
            # Calculate duration of start time to stop time
            if self.start_time and self.end_time:
                self.data.append(TimeObj(self.start_time, self.end_time))
                self.start_time = None
                self.end_time = None
                self.update_ui()

            event.GetEventObject().SetLabel("Click to Start Timer")

        #   def WriteNotesCheck(self):
        """
        This function is just a 15 minute timer to remind you to stop and write down any notes. This should only
        be running when the OnToggle state is true
        :return: Nothing
        - Start a 15 minute timer
        - At the 15 minute mark, notify the user to make notes
        - Have a button to acknowledge the task is done
        - Start the timer over again
        """

    def OnQuit(self, e):
        self.Close()

    def update_ui(self):
        self.dataOlv.SetColumns(
            [
                ColumnDefn("Start Time", "left", 150, "start"),
                ColumnDefn("End Time", "left", 150, "end"),
                ColumnDefn("Duration", "left", 200, "duration"),
            ]
        )
        self.dataOlv.SetObjects(self.data)


class MainFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="Time Tracker")
        panel = TimePanel(self)
        self.Show()


def main():
    app = wx.App(redirect=False)
    frame = MainFrame()
    app.MainLoop()


if __name__ == "__main__":
    main()
